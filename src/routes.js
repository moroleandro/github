import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import Main from "./pages/Main";
import User from "./pages/User";

const Routes = createAppContainer(
  createStackNavigator(
    {
      Main: {
        screen: Main,
        navigationOptions: {
          title: "GitHub"
        }
      },
      User: {
        screen: User,
        navigationOptions: {
          title: "Perfil do Github"
        }
      }
    },
    {
      defaultNavigationOptions: {
        headerTintColor: "#FFF",
        headerBackTitleVisible: false,
        headerStyle: {
          backgroundColor: "#24292E"
        }
      }
    }
  )
);

export default Routes;
